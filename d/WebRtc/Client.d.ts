import { Client as IClient, RtcFactory } from '../Interface';
import { Peer } from './Peer';
import { Room } from './Room';
export declare class Client implements IClient {
    protected rtcFactory: RtcFactory;
    protected eventListeners: any[];
    protected uid: number;
    protected peers: {
        [id: number]: Peer;
    };
    protected rooms: {
        [id: string]: Room;
    };
    protected server: SocketIOClient.Socket;
    protected connections: any;
    GetPeer(id: number): Peer;
    private static instances;
    static Get(stunServer: string, rtcFactory: RtcFactory): Client;
    static Dispose(): void;
    private constructor();
    private AddPeer(id);
    Room(id: string): Room;
    NewConnection(peer: Peer): void;
    HandleEvent(e: any): void;
    On(event: string, callback: (data: any, sender: Peer) => void): void;
    EmitTo(peer: Peer, event: string, data: any): void;
    HandleDirectEvent(event: any): void;
    Disconnect(): void;
}
