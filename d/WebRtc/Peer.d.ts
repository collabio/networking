import { Peer as IPeer } from '../Interface';
export declare class Peer implements IPeer {
    protected _client: any;
    protected _connection: any;
    protected _channel: any;
    protected id: number;
    channel: any;
    readonly Id: number;
    readonly IsReady: boolean;
    constructor(_client: any, _connection: any, _channel: any, id: number);
    Emit(event: string, room: string, namespace: string, data: any): void;
    EmitDirect(sender: number, event: string, data: any): void;
}
