import { Namespace as INamespace } from '../Interface';
import { Room } from './Room';
import { Peer } from './Peer';
export declare class Namespace implements INamespace {
    protected stun: SocketIOClient.Socket;
    protected globalPeers: any;
    protected room: Room;
    protected name: string;
    protected eventListeners: any[];
    protected onNewConnection: () => void;
    protected peers: number[];
    readonly Id: string;
    readonly Peers: Peer[];
    constructor(stun: SocketIOClient.Socket, globalPeers: any, room: Room, name: string);
    AddMember(id: number): void;
    RemoveMember(id: number): void;
    Broadcast(event: any, data: any): void;
    On(event: any, callback: any): void;
    HandleEvent(event: any): void;
}
