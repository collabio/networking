import { Room as IRoom } from '../Interface';
import { Namespace } from './Namespace';
export declare class Room implements IRoom {
    protected stun: SocketIOClient.Socket;
    protected peers: any;
    protected id: string;
    protected namespaces: {
        [id: string]: Namespace;
    };
    readonly Id: string;
    readonly Namespaces: {
        [id: string]: Namespace;
    };
    constructor(stun: SocketIOClient.Socket, peers: any, id: string);
    Namespace(name: string): Namespace;
}
