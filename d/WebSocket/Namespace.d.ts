import { Namespace as INamespace } from '../Interface';
import { Room as IRoom } from '../Interface';
import { Peer } from './Peer';
export declare class Namespace implements INamespace {
    protected server: SocketIOClient.Socket;
    protected room: IRoom;
    protected name: string;
    protected peers: Peer[];
    protected onNewPeer: any;
    readonly Peers: Peer[];
    constructor(server: SocketIOClient.Socket, room: IRoom, name: string);
    Broadcast(event: any, data: any): void;
    On(event: any, callback: any): any;
}
