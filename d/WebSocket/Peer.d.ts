import { Peer as IPeer } from '../Interface';
export declare class Peer implements IPeer {
    protected id: any;
    readonly Id: number;
    constructor(id: any);
    Emit(event: string, room: string, namespace: string, data: any): void;
    EmitDirect(sender: number, event: string, data: any): void;
}
