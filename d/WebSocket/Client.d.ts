import { Client as IClient } from '../Interface';
import { Room } from './Room';
import { Peer } from './Peer';
export declare class Client implements IClient {
    protected uid: number;
    protected peers: {
        [id: number]: Peer;
    };
    protected rooms: {
        [id: string]: Room;
    };
    protected server: SocketIOClient.Socket;
    private static instances;
    static Get(relayServer: string): Client;
    static Dispose(): void;
    private constructor();
    Room(id: string): Room;
    Disconnect(): void;
    On(event: string, callback: (data: any, sender: Peer) => void): void;
    EmitTo(peer: Peer, event: string, data: any): void;
}
