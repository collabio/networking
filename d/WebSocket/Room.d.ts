import { Room as IRoom } from '../Interface';
import { Namespace } from './Namespace';
export declare class Room implements IRoom {
    protected server: SocketIOClient.Socket;
    protected id: string;
    protected namespaces: {
        [id: string]: Namespace;
    };
    readonly Id: string;
    constructor(server: SocketIOClient.Socket, id: string);
    Namespace(ns: string): Namespace;
}
