import { HttpClient } from '../Interface';
export declare class ApiClient {
    protected httpClient: HttpClient;
    protected url: string;
    protected headers: any;
    AuthToken: string;
    constructor(httpClient: HttpClient, url: string);
    Get(path: string): any;
    Post(path: string, data: any): any;
    Put(path: string, data: any): any;
    Patch(path: string, data: any): any;
    Delete(path: string): any;
    private HandleError(data);
}
