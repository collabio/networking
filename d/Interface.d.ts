export interface Peer {
    Emit(event: string, room: string, namespace: string, data: any): any;
    EmitDirect(sender: number, event: string, data: any): any;
    Id: number;
}
export interface Client {
    Room(id: string): Room;
    Disconnect(): any;
    On(event: string, callback: (data: any, sender: Peer) => void): any;
    EmitTo(peer: Peer, event: string, data: any): any;
}
export interface Namespace {
    Broadcast(event: string, data: any): any;
    On(event: string, callback: (data: any) => void): any;
    Peers: Peer[];
}
export interface Room {
    Id: string;
    Namespace(namespace: string): Namespace;
}
export interface RtcFactory {
    CreatePeerConnection(): any;
}
export interface HttpClient {
    Request(url: string, path: string, method: string, headers?: any, data?: any): any;
}
