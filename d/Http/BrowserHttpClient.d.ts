import { HttpClient } from '../Interface';
export declare class BrowserHttpClient implements HttpClient {
    Request(url: string, path: string, method: string, headers?: {}, data?: any): Promise<any>;
}
