import { HttpClient } from '../Interface';
export declare class NodeHttpClient implements HttpClient {
    Request(url: string, path: string, method: string, headers?: {}, data?: any): any;
}
