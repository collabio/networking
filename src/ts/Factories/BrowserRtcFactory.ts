declare var webkitRTCPeerConnection;

export class RtcFactory {
    public CreatePeerConnection() {
        return new webkitRTCPeerConnection(null, null);
    }
}