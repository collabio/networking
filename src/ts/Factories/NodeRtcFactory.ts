var RTCPeerConnection = require('wrtc').RTCPeerConnection;

export class RtcFactory {
    public CreatePeerConnection() {
        return new RTCPeerConnection(null, null);
    }
}  