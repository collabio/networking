import { HttpClient } from '../Interface';

export class ApiClient {
    protected headers;

    public set AuthToken(value: string) {
        this.headers.Cookie = `authToken=${value}`;
    }

    public constructor(protected httpClient: HttpClient,
        protected url: string) {
        this.headers = {};
    }

    public Get(path: string) {
        return this.httpClient.Request(this.url, path, 'GET', this.headers).then(this.HandleError);
    }

    public Post(path: string, data) {
        return this.httpClient.Request(this.url, path, 'POST', this.headers, data).then(this.HandleError);
    }

    public Put(path: string, data) {
        return this.httpClient.Request(this.url, path, 'PUT', this.headers, data).then(this.HandleError);
    }

    public Patch(path: string, data) {
        return this.httpClient.Request(this.url, path, 'PATCH', this.headers, data).then(this.HandleError);
    }

    public Delete(path: string) {
        return this.httpClient.Request(this.url, path, 'DELETE', this.headers).then(this.HandleError);
    }

    private HandleError(data) {
        if (data.error)
            return Promise.reject(data.error);
        return data;
    }
}