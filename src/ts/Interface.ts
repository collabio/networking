export interface Peer {
    Emit(event: string, room: string, namespace: string, data: any);
    EmitDirect(sender: number, event: string, data: any);
    Id: number;
}

export interface Client {
    Room(id: string): Room;
    Disconnect();
    On(event: string, callback: (data: any, sender: Peer) => void);
    EmitTo(peer: Peer, event: string, data: any)
}

export interface Namespace {
    Broadcast(event: string, data: any);
    On(event: string, callback: (data: any) => void);
    Peers: Peer[];
}

export interface Room {
    Id: string;
    Namespace(namespace: string): Namespace;
}

export interface RtcFactory {
    CreatePeerConnection();
}

export interface HttpClient {
    Request(url: string, path: string, method: string, headers?, data?);
}