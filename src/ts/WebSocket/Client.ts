import { Client as IClient } from '../Interface';
import { Room } from './Room';
import { Peer } from './Peer';
import * as io from 'socket.io-client';

export class Client implements IClient {

    protected uid: number;
    protected peers: { [id: number]: Peer };
    protected rooms: { [id: string]: Room };

    protected server: SocketIOClient.Socket;

    private static instances: { [server: string]: Client } = {};

    public static Get(relayServer: string) {
        if (!Client.instances[relayServer])
            Client.instances[relayServer] = new Client(relayServer);

        return Client.instances[relayServer];
    }
    public static Dispose() {
        for(var x in Client.instances)
            Client.instances[x].Disconnect();
            
        Client.instances = {};
    }

    private constructor(relayServer: string) {
        this.server = io(relayServer);
        this.peers = {};
        this.rooms = {};

        this.server.on('addToRoom', (data) => {
            if (!this.peers[data.peer])
                this.peers[data.peer] = new Peer(data.peer);
        });

        this.server.on('removeFromRoom', (data) => {
            if (this.peers[data.peer])
                delete this.peers[data.peer];
        });
    }

    Room(id: string): Room {
        if (!this.rooms[id])
            this.rooms[id] = new Room(this.server, id);
        return this.rooms[id];
    }

    Disconnect() {
        this.server.close();
    }

    On(event: string, callback: (data: any, sender: Peer) => void) {
        this.server.on(event, direct => {
            if (direct && this.peers[direct.sender])
                callback(direct.data, this.peers[direct.sender]);
        });
    }

    EmitTo(peer: Peer, event: string, data: any) {
        this.server.emit('direct', { event: event, peer: peer.Id, data: data });
    }
}