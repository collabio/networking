import { Namespace as INamespace } from '../Interface';
import { Room as IRoom } from '../Interface';
import { Peer } from './Peer';

export class Namespace implements INamespace {

    protected peers: Peer[];
    protected onNewPeer;

    public get Peers(): Peer[] {
        return this.peers;
    }

    public constructor(protected server: SocketIOClient.Socket,
        protected room: IRoom,
        protected name: string) {

        this.peers = [];
        this.onNewPeer = [];

        this.server.emit('join', { room: room.Id, ns: name });

        this.server.on('addToRoom', (data) => {
            let peer = new Peer(data.peer);
            this.peers.push(peer);
            this.onNewPeer.forEach(callback => callback(peer));
        });

        this.server.on('removeFromRoom', (data) => {
            this.peers.forEach((peer, index) => {
                if(peer.Id == data.peer)
                    return this.peers.splice(index, 1);
            })
            console.log('#' + data.peer + ' left');
        });
    }

    Broadcast(event, data) {
        this.server.emit('broadcast', { event: event, room: this.room.Id, ns: this.name, data: data });
    }

    On(event, callback) {
        if (event == 'newPeer')
            return this.onNewPeer.push(callback);

        this.server.on(event, broadcast => {
            if (broadcast.room == this.room.Id && broadcast.ns == this.name)
                callback(broadcast.data);
        });
    }
}