import { Room as IRoom } from '../Interface';
import { Namespace } from './Namespace';

export class Room implements IRoom {

    protected namespaces: { [id: string]: Namespace };

    public get Id(): string {
        return this.id;
    }

    public constructor(protected server: SocketIOClient.Socket,
                       protected id: string) {
        this.namespaces = {};
    }

    Namespace(ns: string): Namespace {
        if(!this.namespaces[ns])
            this.namespaces[ns] = new Namespace(this.server, this, ns);
        return this.namespaces[ns];
    }
}