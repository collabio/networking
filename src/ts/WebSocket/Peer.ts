import { Peer as IPeer } from '../Interface';

export class Peer implements IPeer {

    public get Id(): number {
        return this.id;
    }

    constructor(protected id) {

    }

    Emit(event: string, room: string, namespace: string, data: any) {
        console.warn('not implemented property WebSocket.Peer.Emit');
    }

    EmitDirect(sender: number, event: string, data: any) {
        console.warn('not implemented property WebSocket.Peer.EmitDirectTo');
    }
}