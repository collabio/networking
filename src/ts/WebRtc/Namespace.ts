import { Namespace as INamespace } from '../Interface';
import { Client } from './Client';
import { Room } from './Room';
import { Peer } from './Peer';

export class Namespace implements INamespace {

    protected eventListeners: any[];
    protected onNewConnection: () => void;
    protected peers: number[];

    public get Id(): string {
        return this.room.Id + '|' + this.name;
    }

    public get Peers(): Peer[] {
        return this.peers
                   .map(peerId => this.globalPeers[peerId])
                   .filter(peer => peer && peer.IsReady);
    }

    public constructor(protected stun: SocketIOClient.Socket,
        protected globalPeers,
        protected room: Room,
        protected name: string) {
        this.eventListeners = [];
        this.peers = [];
        stun.emit('join', { room: room.Id, namespace: name });
    }

    AddMember(id: number) {
        this.peers.push(id);
        if (this.globalPeers[id] && this.globalPeers[id].IsReady)
            this.HandleEvent({ name: 'newPeer', data: id });
    }

    RemoveMember(id: number) {
        this.peers.splice(this.peers.indexOf(id), 1);
        this.HandleEvent({ name: 'peerLeft', data: id });
    }

    Broadcast(event, data) {
        for (let peer of this.peers)
            if (this.globalPeers[peer].IsReady)
                this.globalPeers[peer].Emit(event, this.room.Id, this.name, data);
    }

    On(event, callback) {
        this.eventListeners.push({ name: event, handler: callback });
    }

    HandleEvent(event) {
        for (let eventListener of this.eventListeners)
            if (eventListener.name == event.name)
                eventListener.handler(event.data);
    }
}