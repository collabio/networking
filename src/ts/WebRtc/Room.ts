import { Room as IRoom } from '../Interface';
import { Namespace } from './Namespace';
import { Client } from './Client';

export class Room implements IRoom {

    protected namespaces: { [id: string]: Namespace };

    public get Id(): string {
        return this.id;
    }

    public get Namespaces(): { [id: string]: Namespace } {
        return this.namespaces;
    }

    public constructor(protected stun: SocketIOClient.Socket,
        protected peers,
        protected id: string) {
        this.namespaces = {};
    }

    Namespace(name: string): Namespace {
        if (!this.namespaces[name])
            this.namespaces[name] = new Namespace(this.stun, this.peers, this, name);
        return this.namespaces[name];
    }
}