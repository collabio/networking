import { Peer as IPeer } from '../Interface';

export class Peer implements IPeer {

    public get channel() {
        return this._channel;
    }
    public set channel(value) {
        this._channel = value;
    }

    public get Id(): number {
        return this.id;
    }

    public get IsReady(): boolean {
        return this._channel && this._channel.readyState == 'open';
    }

    constructor(protected _client, protected _connection, protected _channel, protected id: number) {
    }

    Emit(event: string, room: string, namespace: string, data: any) {
        this._channel.send(JSON.stringify({ name: event, room: room, namespace: namespace, data: data }));
    }

    EmitDirect(sender: number, event: string, data: any) {
        this._channel.send(JSON.stringify({ name: event, sender: sender, data: data }));
    }
}