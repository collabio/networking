import { Client as IClient, RtcFactory } from '../Interface';
import { Peer } from './Peer';
import { Room } from './Room';
import * as io from 'socket.io-client';

export class Client implements IClient {

    protected eventListeners: any[];

    protected uid: number;
    protected peers: { [id: number]: Peer };
    protected rooms: { [id: string]: Room };

    protected server: SocketIOClient.Socket;
    protected connections: any;

    public GetPeer(id: number): Peer {
        return this.peers[id];
    }

    private static instances: { [server: string]: Client } = {};

    public static Get(stunServer: string, rtcFactory: RtcFactory) {
        if (!Client.instances[stunServer])
            Client.instances[stunServer] = new Client(stunServer, rtcFactory);

        return Client.instances[stunServer];
    }
    public static Dispose() {
        for(var x in Client.instances)
            Client.instances[x].Disconnect();
            
        Client.instances = {};
    }

    private constructor(stunServer: string,
        protected rtcFactory: RtcFactory) {
        this.server = io(stunServer);

        //this.server.emit('discover');

        this.server.on('joined', uniqId => {
            this.uid = uniqId;
        });

        this.peers = {};
        this.rooms = {};
        this.connections = {};
        this.eventListeners = [];

        this.server.on('user', this.AddPeer);
        this.server.on('room', room => {
            for (let member of room.members) {
                let peer = this.peers[member] == null ? this.AddPeer(member)
                    : this.peers[member];
                this.rooms[room.id].Namespaces[room.ns].AddMember(member);
            }
        });
        this.server.on('addToRoom', data => {
            this.rooms[data.room.room].Namespaces[data.room.namespace].AddMember(data.user);
        });

        this.server.on('accept', data => {
            this.connections[data.user].setRemoteDescription(data.description);
        });
        this.server.on('addIceCandidate', data => {
            this.connections[data.user].addIceCandidate(data.candidate);
        });

        this.server.on('newConnection', data => {
            var localConnection = this.connections[data.user] = rtcFactory.CreatePeerConnection();
            var receiveChannel;
            let peer = new Peer(this, localConnection, null, data.user);
            this.peers[data.user] = peer;

            localConnection.onconnectionstatechange = (e) => console.log(e);

            localConnection.onicecandidate = event => {
                if (event.candidate) {
                    this.server.emit('addIceCandidate', {
                        user: data.user,
                        candidate: event.candidate
                    });
                }
            };

            localConnection.ondatachannel = event => {
                receiveChannel = peer.channel = event.channel;

                receiveChannel.onopen = () => {
                    this.NewConnection(peer);
                };
                receiveChannel.onclose = () => {
                    console.log('channel closed');
                };
                receiveChannel.onerror = (e) => {
                    console.log(e);
                };
                receiveChannel.onmessage = e => this.HandleEvent(e);
            };
            localConnection.setRemoteDescription(data.description);
            localConnection.createAnswer().then(desc => {
                localConnection.setLocalDescription(desc);
                this.server.emit('accept', {
                    user: data.user,
                    description: desc
                });
            });
        });
        this.server.on('remove', data => {
            for (let room in this.rooms)
                for (let ns in this.rooms[room].Namespaces)
                    this.rooms[room].Namespaces[ns].RemoveMember(data.user);
            delete this.connections[data.user];
            delete this.peers[data.user];
        });
    }

    private AddPeer(id: number): Peer {
        var localConnection = this.connections[id] = this.rtcFactory.CreatePeerConnection();
        var sendChannel = localConnection.createDataChannel('sendDataChannel', null);
        let peer = new Peer(this, localConnection, sendChannel, id);
        this.peers[id] = peer;

        localConnection.onicecandidate = event => {
            if (event.candidate) {
                this.server.emit('addIceCandidate', {
                    user: id,
                    candidate: event.candidate
                });
            }
        };
        localConnection.onconnectionstatechange = (e) => console.log(e);

        sendChannel.onopen = () => {
            this.NewConnection(peer);
        };
        sendChannel.onclose = () => {
            console.log('channel closed');
        };
        sendChannel.onerror = (e) => {
            console.log(e);
        };
        sendChannel.onmessage = e => this.HandleEvent(e);

        localConnection.createOffer().then(desc => {
            localConnection.setLocalDescription(desc);
            this.server.emit('connectWith', {
                user: id,
                description: desc
            });
        });
        return peer;
    }

    Room(id: string): Room {
        if (!this.rooms[id])
            this.rooms[id] = new Room(this.server, this.peers, id);
        return this.rooms[id];
    }

    NewConnection(peer: Peer) {
        for (let id in this.rooms)
            for (let ns in this.rooms[id].Namespaces)
                for (let p of this.rooms[id].Namespaces[ns].Peers)
                    if (p.Id == peer.Id)
                        this.rooms[id].Namespaces[ns].HandleEvent({ name: 'newPeer', data: peer.Id });

    }

    HandleEvent(e) {
        var event = JSON.parse(e.data);
        if (event.room)
            this.rooms[event.room].Namespaces[event.namespace].HandleEvent(event);
        else if (event.sender)
            this.HandleDirectEvent(event);
    }

    On(event: string, callback: (data: any, sender: Peer) => void) {
        this.eventListeners.push({ name: event, handler: callback });
    }

    EmitTo(peer: Peer, event: string, data: any) {
        peer.EmitDirect(this.uid, event, data);
    }

    HandleDirectEvent(event) {
        for (let eventListener of this.eventListeners)
            if (eventListener.name == event.name)
                eventListener.handler(event.data, this.peers[event.sender]);
    }

    Disconnect() {
        for (let connection in this.connections)
            if (this.connections[connection] && this.connections[connection].signalingState != 'closed')
                this.connections[connection].close();

        this.server.disconnect();
    }
}