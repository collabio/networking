var Http = require('http');
var Buffer = require('buffer');
var Url = require('url');
import { HttpClient } from '../Interface';

export class NodeHttpClient implements HttpClient {

    public Request(url: string, path: string, method: string, headers = {}, data?) {
        return new Promise(resolve => {
            let postData;

            if (data) {
                postData = JSON.stringify(data);
                headers['Content-Type'] = 'application/json';
                headers['Content-Length'] = Buffer.byteLength(postData);
            }

            let urlData = Url.parse(url);
            let options = {
                hostname: urlData.hostname,
                port: urlData.port,
                path: urlData.pathname + path,
                method,
                headers
            };
            let request = Http.request(options, res => {
                let response = '';
                res.setEncoding('utf8');
                res.on('data', chunk => {
                    response += chunk;
                });
                res.on('end', () => {
                    resolve(JSON.parse(response));
                });
            });

            if (data)
                request.write(postData);
            request.end();
        });
    }

}