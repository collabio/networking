import { HttpClient } from '../Interface';

export class BrowserHttpClient implements HttpClient {

    public Request(url: string, path: string, method: string, headers = {}, data?) {

        let options: any = {
            method,
            headers,
            credentials: 'include'
        };

        if (data) {
            options.body = JSON.stringify(data);
            options.headers['Content-Type'] = 'application/json; charset=utf-8';
        }

        return fetch(url + path, options)
            .then<any>(response => response.json());
    }

}